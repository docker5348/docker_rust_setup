FROM ubuntu:24.04
RUN apt update -y && apt upgrade -y && apt install -y curl \ 
    build-essential gcc \ 
    wget \
    git \ 
    openssl \ 
    libssl-dev \
    librust-libudev-sys-dev \
    pkg-config; \
    curl https://sh.rustup.rs -sSf | bash -s -- -y;
ENV PATH="/root/.cargo/bin:${PATH}"
RUN echo 'source $HOME/.cargo/env' >> $HOME/.bashrc; \
    rustup self update && rustup update stable && \ 
    rustup toolchain install nightly; \
    rustup completions bash cargo > $HOME/cargo_complete; \
    rustup component add llvm-tools-preview; \
    rustup target add thumbv6m-none-eabi; \
    cargo +nightly install --debug --locked --all-features --no-track --verbose -Z sparse-registry \
    cargo-outdated maturin \
    cargo-generate \
    evcxr_repl \
    mdbook \
    elf2uf2-rs
