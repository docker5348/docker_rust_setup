mkdir -p $HOME/projects/rust
docker build -t "setup_rust:Dockerfile" . ;
docker run \
	-it \
	--privileged \
  --rm \
  --name ferris \
	--net host \
	--ipc host \
	-e DISPLAY=$DISPLAY \
  -e USER=$USER \
	-v ~/projects/rust/:/rust_projects \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
	setup_rust:Dockerfile

